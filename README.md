## Propuesta  de especificación del API REST *Mapfeed* para consumo de datos de la UA que contengan referencias a códigos SIGUA ##

**Mapfeed** es una propuesta de API REST que permita obtener datos corporativos de la Universidad de Alicante que estén referidos a una localización geográfica expresada como un código válido del Sistema de Información Geográfica de la universidad de Alicante (SIGUA).

### Contenido de este repositorio ###

La propuesta de especificación se almacena en el documento *specs.md*. Todos los usuarios de la organización *SIGUA* pueden colaborar diréctamente en su redacción.

### Guía para los autores ###

* La especificación se estructura en secciones. Cada sección se corresponde con un tipo de recurso.
* El tipo de recurso se define con:  
    * Nombre
    * Breve descripción
    * Bloque JSON Schema
    * Ejemplo JSON
* En cada sección se deben proponer recursos concretos que devuelvan una respuesta del tipo descrito.