# Especificación del API REST *Mapfeed* de la Universidad de Alicante
## Familia
**Nombre de recurso:** */mapfeed/familia*
  
**Descripción:**  
Familias bajo las que se catalogan los elementos del inventario del Servicio de Prevención
  
**Esquema:**

    {
        "type": "object",
        "description": "Familia del inventario del Servicio de Prevención",
        "properties": {
            "id_familia": {
                "type": "string",
                "description": "Identificador de familia"
            },
            "desc_familia": {
                "type": "string",
                "description": "Descripción de familia"
            },
            "subfamilias": {
                "type": "array",
                "description": "Array de subfamilias",
                "uniqueItems": true,
                "items": {
                    "type": "object",
                    "description": "Subfamilia del inventario del Servicio de Prevención",
                    "properties": {
                        "id_subfamilia": {
                            "type": "string",
                            "description": "Identificador de subfamilia"
                        },
                        "desc_subfamilia": {
                            "type": "string",
                            "description": "Descripción de subfamilia"
                        }
                    }
                }
            }
        }
    }

**Ejemplo:**

    {
        "id_familia": "F001",
        "desc_familia": "INSTALACIONES CONTRA INCENDIOS",
        "subfamilias": [
            {
                "id_subfamilia": "F001S0001",
                "desc_subfamilia": "EXTINTORES"
            }
        ]
    }
  
**GET Requests:**

- Recupera todas las familias y sus respectivas subfamilias

        /mapfeed/familia/all/items

- Recupera todas las subfamilias de una familia por *id_familia*

        /mapfeed/familia/:id_familia

---

##Equipo
**Nombre de recurso:** */mapfeed/equipo*
  
**Descripción:**  
Equipos inventariados por el Servicio de Prevención
  
**Esquema:**

    {
        "type": "object",
        "description": "Equipo inventariado por el Servicio de Prevención",
        "properties": {
            "id_equipo": {
                "type": "string",
                "description": "Identificador del equipo"
            },
            "codigo_sigua": {
                "type": "string",
                "description": "Código SIGUA de la estancia"
            },
            "descripcion": {
                "type": "string",
                "description": "Descripción del equipo"
            },
            "subfamilia": {
                "type": "object",
                "description": "Subfamilia del inventario del Servicio de Prevención",
                "properties": {
                    "id_subfamilia": {
                        "type": "string",
                        "description": "Identificador de subfamilia"
                    },
                    "desc_subfamilia": {
                        "type": "string",
                        "description": "Descripción de subfamilia",
                    },
                    "familia": {
                        "type": "object",
                        "description": "",
                        "properties": {
                            "id_familia": {
                                "type": "string",
                                "description": "Identificador de familia"
                            },
                            "desc_familia": {
                                "type": "string",
                                "description": "Descripción de familia"
                            }
                        }
                    }
                }
            }
        }
    }

**Ejemplo:**

    {
        "id_equipo": "1001",
        "codigo_sigua": "0037P1075",
        "descripcion": "EXTINTOR PORTÁTIL",
        "subfamilia": {
            "id_subfamilia": "F001S0001",
            "desc_subfamilia": "EXTINTORES",
            "familia": {
                "id_familia": "F001",
                "desc_familia": "INSTALACIONES CONTRA INCENDIOS"
            }
        }
    }
  
**GET Requests:**

- Recupera un equipo por *id_equipo*

        /mapfeed/equipo/:id_equipo

- Recupera todos los equipos de una estancia por *codigo_sigua*

        /mapfeed/equipo/estancia/:codigo_sigua/items

- Recupera todos los equipos de un edificio por *codigo_edificio*

        /mapfeed/equipo/edificio/:codigo_edificio/items

- Recupera todos los equipos de una familia por *id_familia*

        /mapfeed/equipo/familia/:id_familia/items

- Recupera todos los equipos de una subfamilia por *id_subfamilia*

        /mapfeed/equipo/subfamilia/:id_subfamilia/items

---

##Sesión
**Nombre de recurso:** */mapfeed/sesion*
  
**Descripción:**  
Sesiones docentes programadas
  
**Esquema:**

    {
        "type": "object",
        "description": "Sesión docente programada en una franja horaria y una estancia concretas",
        "properties": {
            "id_sesion": {
                "type": "string",
                "description": "Identificador de sesión"
            },
            "codigo_sigua": {
                "type": "string",
                "description": "Código SIGUA de la estancia"
            },
            "franja": {
                "type": "object",
                "description": "Franja horaria",
                "properties": {
                    "inicio": {
                        "type": "string",
                        "description": "Fecha y hora de comienzo normalizadas según RFC 3339"
                    },
                    "fin": {
                        "type": "string",
                        "description": "Fecha y hora de finalización normalizadas según RFC 3339"
                    }
                }
            },
            "asignatura": {
                "type": "object",
                "description": "Asignatura que se imparte",
                "properties": {
                    "id_asignatura": {
                        "type": "string",
                        "description": "Código de asignatura"
                    },
                    "desc_asignatura": {
                        "type": "string",
                        "description": "Denominación de la asignatura"
                    },
                    "docentes": {
                        "type": "array",
                        "description": "Lista de códigos de persona de la UA correspondientes al equipo docente",
                        "uniqueItems": true,
                        "items": {
                            "type": "integer",
                            "description": "Código interno de persona propio de la UA"
                        }
                    }                    
                }
            },
            "aforo": {
                "type": "integer",
                "description": "Total de alumnos registrados"
            }
        }
    }

**Ejemplo:**

    {
        "id_sesion": "abc",
        "codigo_sigua": "0037P1015",
        "franja": {
            "inicio": "[YYYY]-[MM]-[DD]T[hh]:[mm][+|-][hh]:[mm]",
            "fin": "[YYYY]-[MM]-[DD]T[hh]:[mm][+|-][hh]:[mm]"
        },
        "asignatura": {
            "id_asignatura": "abc",
            "desc_asignatura": "abc",
            "docentes": [66969,68404]
        },
        "aforo": 0
    }
  
**GET Requests:**

- Recupera una sesion por *id_sesion*

        /mapfeed/sesion/:id_sesion

- Recupera todas las sesiones programadas en una estancia por *codigo_sigua*

        /mapfeed/sesion/estancia/:codigo_sigua/items

- Recupera todas las sesiones programadas en un edificio por *codigo_edificio*

        /mapfeed/sesion/edificio/:codigo_edificio/items

- Recupera todas las sesiones programadas para una asignatura por *id_asignatura*

        /mapfeed/sesion/asignatura/:id_asignatura/items

---
